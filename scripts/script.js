//EDIT THESE VALUES 
// images must following the naming convention "1.jpg", "2.jpg", "3.jpg", etc
const imageCount = 72;
const imageFolder = "images/smokey/";
const imageFormat = ".jpg";

//Preload all our images
PreloadImages(1);

function PreloadImages(count)
{
    var img = new Image();
    var value = parseInt(count);
    img.src = imageFolder + value + imageFormat;
    //preload the next image
    if(count < imageCount)
    {
        count++;
        img.onload = PreloadImages(count);
    }
    //create the waypoints when everything is loaded
    else{
        img.onload = CreateAllWaypoints;
    }
}


function CreateAllWaypoints(){
    //the element that will display one image at a time
    const flipbook = document.getElementById("flipbook");
    //replace the loading gif after all images have loaded
    flipbook.src = imageFolder + 1 + imageFormat;

    //get the unordered list element by id
    const list = document.getElementById("waypoint-list");

    //generate a bunch of list items with their own ids. Each one will be assigned as a Waypoint element
    for(i = 1; i <= imageCount; i++)
    {
        newItem = document.createElement('li');
        newItem.setAttribute("id", "step-"+i);
        newItem.textContent = i;
        list.appendChild(newItem);
        CreateNewWaypoint(newItem, i);
    }

    //custom waypoints
    CreateCheckPoint1();
    CreateCheckPoint2();
    CreateCheckPoint3();
    CreateCheckPoint4();
    CreateCheckPoint5();
}

//create a Waypoint object
function CreateNewWaypoint(e, count){
    var value = parseInt(count);
    new Waypoint({
        element: e,
        handler: function(direction){
            flipbook.src = imageFolder + value + imageFormat;
            
        }
    })
}

function CreateCheckPoint1(){
    const trigger = document.getElementById('step-21');
    new Waypoint({
        element: trigger,
        handler: function(direction){
            alert("hello world!");
        }
    })
}

function CreateCheckPoint2(){
    const trigger = document.getElementById('step-24');
    new Waypoint({
        element: trigger,
        handler: function(direction){
            alert("hello world!");
        }
    })
}
